package com.jobo

import akka.actor.ActorSystem
import akka.event.{Logging, LoggingAdapter}
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.server.Directives
import akka.stream.{ActorMaterializer, Materializer}
import com.typesafe.config.{Config, ConfigFactory}
import slick.jdbc.{JdbcProfile, PostgresProfile}
import spray.json.DefaultJsonProtocol

import scala.concurrent.{ExecutionContextExecutor, Future}


case class Code(code: String)

trait Protocol extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val codeFormat = jsonFormat1(Code.apply)
}

trait Service extends Directives with Protocol with IbanRepository {
  implicit val system: ActorSystem
  implicit def executor: ExecutionContextExecutor
  implicit val materializer: Materializer

  val logger: LoggingAdapter

  val routes = {
    logRequestResult("simple-backend") {
      (get & path(Segment)) { code =>
        complete {
          Code(code)
        }
      } ~
      (get & path(Segment / "iban")) { code =>
        complete {
          Code(code)
        }
      }
    }
  }
}

object SimpleApp extends App with Service {
  override implicit val system = ActorSystem()
  override implicit val executor = system.dispatcher
  override implicit val materializer = ActorMaterializer()

  val config = ConfigFactory.load()
  override val logger = Logging(system, getClass)


  override val url = config.getString("database.url")
  override val username = config.getString("database.user")
  override val password = config.getString("database.password")



  override val driver = PostgresProfile

  migrateDatabaseSchema()

  import driver.api._

  val db = Database.forDataSource(dataSource, maxConnections = None)
  db.createSession()

  Http().bindAndHandle(routes, config.getString("http.interface"), config.getInt("http.port"))
}

trait Database extends FlywayService {
  import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
  val driver: JdbcProfile

  private val hikariConfig = new HikariConfig()
  hikariConfig.setJdbcUrl(url)
  hikariConfig.setUsername(username)
  hikariConfig.setPassword(password)

  lazy val dataSource = new HikariDataSource(hikariConfig)

}

case class Iban(id: Option[Int], code: String)

trait IbanRepository extends Database {

  import driver.api._

  class Ibans(tag: Tag) extends Table[Iban](tag, "ibans") {
    def id: Rep[Option[Int]] = column[Option[Int]]("id", O.PrimaryKey, O.AutoInc)
    def title: Rep[String] = column[String]("code")

    def * = (id, title) <> ((Iban.apply _).tupled, Iban.unapply)
  }

  protected val ibans = TableQuery[Ibans]

//  def getAll(): Future[Seq[Iban]] =
//    db.run(ibans.result)
}

trait FlywayService {
  import org.flywaydb.core.Flyway

  val url: String
  val username: String
  val password: String

  private[this] val flyway = new Flyway()

  def migrateDatabaseSchema() : Unit = {
    flyway.setDataSource( url, username, password)
    flyway.migrate()
  }

  def dropDatabase() : Unit = flyway.clean()
}