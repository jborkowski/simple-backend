name := "simple-backend"

organization := "com.jobo"

version := "1.0"

scalaVersion := "2.12.2"

libraryDependencies ++= {
  val akkaHttpVersion = "10.0.7"
  val akkaVersion = "2.4.16"
  val scalaTestVersion = "3.0.1"
  val slickVersion = "3.2.0"

  Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-stream" % akkaVersion,
    "com.typesafe.akka" %% "akka-testkit" % akkaVersion,
    "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion,
    "org.scalatest"     %% "scalatest" % scalaTestVersion % "test",

    "com.typesafe.slick" %% "slick" % slickVersion,
    "org.slf4j" % "slf4j-nop" % "1.6.4",
    "com.typesafe.slick" %% "slick-hikaricp" % "3.2.0",
    "org.flywaydb" % "flyway-core" % "3.2.1",
    "org.postgresql" % "postgresql" % "9.4-1201-jdbc41"
  )
}

        